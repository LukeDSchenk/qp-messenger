mod mongops;
mod utils;

use mongodb::bson::{Bson, oid::ObjectId};
use mongodb::sync::Client;
use mongops::users::CreateUserError;
use mongops::convos::{self as Convos, NewConvoError};
use mongops::messages::{self as Mess, NewMessageError};
use Mess::Message;

fn test_create_user(client: &Client) {
    // create a new user with the specified username and password
    let new_id = match mongops::users::create_user(&client, "a_newly_validated_user", "fuckingAwfulPassword") {
        Ok(user) => user,
        Err(error) => match error {
            CreateUserError::Valid(err) => panic!("{}", err),
            CreateUserError::Mdb(err) => panic!("{}", err),
        }
    };
    let new_id = match new_id.inserted_id {
        Bson::ObjectId(id) => id.to_hex(), // to_hex() turns ObjectId struct to hex str
        _ => panic!("expected an ObjectId, got something else")
    };
    println!("New user created with ID: {}", new_id);
}

fn test_get_user(client: &Client) {
    // normal user who exists
    let u = "nineofpine";
    match mongops::users::get_user(client, u) {
        Ok(doc_option) => match doc_option {
            Some(document) => {
                if let Some(password) = document.get("password").and_then(Bson::as_str) {
                    println!("{}'s password is {}", u, password);
                } else {
                    println!("{} does not have a password, some shit is fucked up", u);
                }
            },
            None => println!("user {} does not exist", u),
        },
        Err(err) => panic!("{}", err),
    };

    // user who has no password for some god damn reason
    let u = "TomCruiseAndGoose";
    match mongops::users::get_user(client, u) {
        Ok(doc_option) => match doc_option {
            Some(document) => {
                if let Some(password) = document.get("password").and_then(Bson::as_str) {
                    println!("{}'s password is {}", u, password);
                } else {
                    println!("{} does not have a password, some shit is fucked up", u);
                }
            },
            None => println!("user {} does not exist", u),
        },
        Err(err) => panic!("{}", err),
    };

    // user who does not exist
    let u = "CaptainMorgan";
    match mongops::users::get_user(client, u) {
        Ok(doc_option) => match doc_option {
            Some(document) => {
                if let Some(password) = document.get("password").and_then(Bson::as_str) {
                    println!("{}'s password is {}", u, password);
                } else {
                    println!("{} does not have a password, some shit is fucked up", u);
                }
            },
            None => println!("user {} does not exist", u),
        },
        Err(err) => panic!("{}", err),
    };
}

fn test_create_convo_fp() {
    let mut vec = vec!["nineofpine", "Joseph","Sparty2k10"];
    let fingerprint = Convos::create_convo_fp(&mut vec);
    println!("{}", fingerprint);

    let mut vec2 = vec!["Sparty2k10", "nineofpine", "Joseph"];
    let fingerprint2 = Convos::create_convo_fp(&mut vec2);
    println!("{}", fingerprint2);

    assert!(fingerprint == fingerprint2, "two vectors should have produced the same fingerprints");
}

fn test_update_user_convos(client: &Client) {
    match Convos::update_user_convos(client, "nineofpine", &ObjectId::with_string("5eed25860701172e81c64a65").unwrap()) {
        Ok(result) => {
            println!("{} document(s) modified", result.modified_count);
        },
        Err(err) => panic!("{}", err)
    };
}

fn test_new_convo(client: &Client) {
    let u1 = "fucka";
    let u2 = "nineofpine";
    let u3 = "Joseph";
    let u4 = "Sparty2k10";
    let u5 = "doooseph";

    let mut usernames = vec![u4, u2, u3];
    match Convos::new_convo(client, usernames) {
        Ok(()) => println!("new conversation created between {}, {}, and {}", u4, u2, u3),
        Err(convo_err) => match convo_err {
            NewConvoError::Dne(err) => panic!("{}", err),
            NewConvoError::Mdb(err) => panic!("{}", err),
            NewConvoError::BadId(err) => panic!("{}", err),
            NewConvoError::UpConvo(err) => panic!("{}", err),
            NewConvoError::ValAcc(err) => panic!("{}", err),
        }
    };
}

fn test_new_message(client: &Client) {
    let message = Message {
        convo_fp: String::from("50171086e17ab6fbc9d5dd3ce864e87bb48abc5c"),
        data: String::from("random encrypted message data, this is not real lololololololololololooooloooool"),
        key_id: String::from("arandomuidofakeythesewillbemadelater"),
        key_start: 2,
        key_end: 17,
    };

    match Mess::new_message(client, message) {
        Ok(new_id) => println!("{}", new_id),
        Err(err) => match err {
            NewMessageError::BadId(err) => panic!("{}", err),
            NewMessageError::Dne(err) => panic!("{}", err),
            NewMessageError::Mdb(err) => panic!("{}", err),
            NewMessageError::UpUnreads(err) => panic!("{}", err),
            NewMessageError::ValAcc(err) => panic!("{}", err),
        },
    };
}

fn main() {
    let mongo_uri = "mongodb://localhost:4444";

    // connect to Mongo at the specified uri
    let client = match mongops::connect(mongo_uri) {
        Ok(mongo_client) => mongo_client,
        Err(error) => panic!("{}", error)
    };
    println!("Successfully connected to MongoDB running at {}", mongo_uri);

    test_create_user(&client);
    //test_user_exists(&client);

    //test_create_convo_fp();

    //test_get_user(&client);

    //test_update_user_convos(&client);
    //test_new_convo(&client);

    //test_new_message(&client);
}
