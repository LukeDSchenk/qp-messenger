use super::{Error, fmt};

#[derive(Debug)]
pub struct BadInsertionError {}

impl Error for BadInsertionError {
    fn description(&self) -> &str {
        "The inserted item returned an incorrect id value type (usually should be ObjectId)."
    }
}

impl fmt::Display for BadInsertionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "The inserted document returned an id with the wrong type (id should be an ObjectId()); Something may be wrong in your database!")
    }
}
