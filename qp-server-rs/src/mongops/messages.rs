use super::*;
use crate::mongops::convos;
use crate::mongops::errors::BadInsertionError;

pub struct Message {
    pub convo_fp: String,
    pub data: String,
    pub key_id: String, // remember, key ids will be changed to random ID's
    pub key_start: u32,
    pub key_end: u32
}

#[derive(Debug)]
pub struct ConvoDneError {
    fp: String,
}

impl Error for ConvoDneError {
    fn description(&self) -> &str {
        "Cannot create messages for a conversation that does not exist"
    }
}

impl fmt::Display for ConvoDneError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Conversation {} does not exist", self.fp)
    }
}

#[derive(Debug)]
pub struct UpdateUnreadsError {
    fp: String,
}

impl Error for UpdateUnreadsError {
    fn description(&self) -> &str {
        "Failed to modify the conversation's user unread maps"
    }
}

impl fmt::Display for UpdateUnreadsError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Unable to modify document for conversation: {}", self.fp)
    }
}

pub enum NewMessageError {
    BadId(BadInsertionError),
    Dne(ConvoDneError),
    Mdb(MdbError),
    UpUnreads(UpdateUnreadsError),
    ValAcc(ValueAccessError),
}

/*
    Steps to creating messages:

    1. Verify the conversation ID given /
    2. Create a new message doc with the given data fields /
    3. Append the ObjectId of the new message to each user's unread array in the conversation doc

    Messages will be deleted when they no longer appear in the unread arrays of any of
    the users involved in the conversation.
*/
pub fn new_message(client: &Client, message: Message) -> Result<ObjectId, NewMessageError> {
    let mut convo_doc = match convos::get_convo(client, &message.convo_fp) {
        Ok(success) => match success {
            Some(document) => document,
            None => return Err(NewMessageError::Dne(ConvoDneError{fp: message.convo_fp})),
        },
        Err(err) => return Err(NewMessageError::Mdb(err)),
    };

    let collection = client.database("qp").collection("messages");
    let message_doc = doc! {
        "convo_fp": &message.convo_fp,
        "data": &message.data,
        "key_id": &message.key_id,
        "key_start": &message.key_start,
        "key_end": &message.key_end,
    };

    let new_message_id = match collection.insert_one(message_doc, None) {
        Ok(new_doc) => {
            match new_doc.inserted_id {
                Bson::ObjectId(id) => id,
                _ => return Err(NewMessageError::BadId(BadInsertionError{}))
            }
        },
        Err(err) => return Err(NewMessageError::Mdb(err)),
    };

    // Maybe move this to a new fn
    // a reference to the "participants" doc within the conversation doc
    let mut participants = match convo_doc.get_document_mut("participants") {
        Ok(doc) => doc,
        Err(err) => return Err(NewMessageError::ValAcc(err)),
    };

    for user in participants.clone().keys() {
        let mut vec = match participants.get_array(user) { // get each user's array as a vec
            Ok(arr) => arr.to_vec(),
            Err(_) => Vec::<Bson>::new(),
        };
        vec.push(Bson::ObjectId(new_message_id.clone())); // append the newly created message

        // figure out better handling of this
        match participants.insert(user, vec) {
            Some(_old) => (), //println!("the old value of {}: was {}", user, old)
            None => (),
        };
    } // now, you should just have to run collection.replace_one(query, convo_doc)

    let collection = client.database("qp").collection("conversations");
    match collection.replace_one(doc!{"fingerprint": &message.convo_fp}, convo_doc, None) {
        Ok(result) => {
            if result.modified_count < 1 {
                return Err(NewMessageError::UpUnreads(UpdateUnreadsError{fp: message.convo_fp}));
            }
        },
        Err(err) => return Err(NewMessageError::Mdb(err)),
    };

    Ok(new_message_id)
}
