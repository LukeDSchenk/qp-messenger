use super::*;
use crate::utils;
use std::collections::HashSet;

/*/// An enum for operating on either ObjectId() objects or username strings in the
/// same funcs.
#[derive(Debug, Clone)]
pub enum UserId {
    Oid(ObjectId),
    Username(String),
}

impl fmt::Display for UserId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            UserId::Oid(obj_id) => write!(f, "UserId: {}", obj_id),
            UserId::Username(username) => write!(f, "UserId: {}", username),
        }
    }
}*/

#[derive(Debug)]
pub struct ValidationError {}

impl Error for ValidationError {
    fn description(&self) -> &str {
        "Usernames should only contain a-z, A-Z, 0-9, _, and be no longer than 23 chars"
    }
}

impl fmt::Display for ValidationError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid username: usernames should only contain a-z, A-Z, 0-9, _, and be no longer than 23 chars")
    }
}

pub enum CreateUserError {
    Valid(ValidationError),
    Mdb(MdbError),
}

/// Validate that a username contains only a-z, A-Z, 0-9, and _.
pub fn username_is_valid(username: &str) -> Result<(), ValidationError> {
    if username.len() > 23 {
        return Err(ValidationError{});
    }

    let chars = vec![
        'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t',
        'u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N',
        'O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7',
        '8','9','_',
    ];

    let mut set = HashSet::new();
    for char in &chars {
        set.insert(char);
    }

    for char in username.chars() {
        if !set.contains(&char) {
            return Err(ValidationError{});
        }
    }
    Ok(())
}

/// Create a new QP user in the MongoDB instance, specifying username and password.
pub fn create_user(client: &Client, username: &str, password: &str) -> Result<InsertOneResult, CreateUserError> {
    match username_is_valid(username) {
        Ok(()) => (),
        Err(err) => return Err(CreateUserError::Valid(err)),
    }

    let collection = client.database("qp").collection("users");
    let doc = doc! {
        "username": username,
        "password": password,
        "conversations": Vec::<Bson>::new()
    };

    match collection.insert_one(doc, None) {
        Ok(res) => Ok(res),
        Err(err) => Err(CreateUserError::Mdb(err)),
    }
}

/// Updated user_exists() fn which simply takes a username String instead and will return the document
/// if a user is found. This fn should make the new_conversation_old fn both more efficient and simpler to work
/// with (won't have to match on UserId enum; won't have to requery the DB after checking if the users exist).
pub fn get_user(client: &Client, username: &str) -> Result<Option<Document>, MdbError> {
    let collection = client.database("qp").collection("users");

    match collection.find_one(doc! { "username": username }, None) {
        Ok(success) => {
            match success {
                Some(document) => Ok(Some(document)),
                None => Ok(None), // Ok meaning the query was successful, None meaning no docs match
            }
        },
        Err(err) => Err(err)
    }
}
