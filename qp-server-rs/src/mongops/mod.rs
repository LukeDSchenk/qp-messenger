pub mod users;
pub mod convos;
pub mod messages;
pub mod errors;

pub use std::{error::Error, fmt};
pub use std::option::Option;
pub use std::vec::Vec;
pub use mongodb::{
    bson::{doc, Bson, oid::ObjectId, oid::Error as OidError},
    bson::document::{Document, ValueAccessError},
    sync::Client,
    error::Error as MdbError,
    results::{InsertOneResult, UpdateResult},
    options::UpdateModifications,
};

/// Connect to a MongoDB instance by specifying a connection URI.
/// To Do: add some logic to actually ping the server and make sure the connection is properly working
/// like you can do in Go.
pub fn connect(mongo_uri: &str) -> Result<Client, MdbError> {
    let client = Client::with_uri_str(mongo_uri)?;
    Ok(client)
}
