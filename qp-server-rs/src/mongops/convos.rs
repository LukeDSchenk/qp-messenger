use super::*;
use crate::utils;
use crate::mongops;
use crate::mongops::errors::BadInsertionError;

///////////////////////////////////////Conversations///////////////////////////////////////////////

// changing UserDneError to hold a username string as well and be easier
#[derive(Debug)]
pub struct UserDneError {
    user: String,
}

impl Error for UserDneError {
    fn description(&self) -> &str {
        "Cannot create a conversation between invalid users"
    }
}

impl fmt::Display for UserDneError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "User {} does not exist", self.user)
    }
}

#[derive(Debug)]
pub struct UpdateConvosError {
    user: String,
    error: MdbError,
}

impl Error for UpdateConvosError {
    fn description(&self) -> &str {
        "An error occurred trying to update a user's conversations list."
    }
}

impl fmt::Display for UpdateConvosError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Unable to update user {}'s conversations array; a solution has not been implemented yet and this must be manually corrected; mongo error: {}", self.user, self.error)
    }
}

pub enum NewConvoError {
    Mdb(MdbError),
    Dne(UserDneError),
    BadId(BadInsertionError),
    UpConvo(UpdateConvosError),
    ValAcc(ValueAccessError),
}

/// Takes a fingerprint String and returns the Document if a convo is found. If it returns Ok(None),
/// then the conversation does not exist.
pub fn get_convo(client: &Client, fingerprint: &str) -> Result<Option<Document>, MdbError> {
    let collection = client.database("qp").collection("conversations");

    match collection.find_one(doc! { "fingerprint": fingerprint }, None) {
        Ok(success) => {
            match success {
                Some(document) => Ok(Some(document)),
                None => Ok(None),
            }
        },
        Err(err) => Err(err)
    }
}

// fn does not need to be pub, just for testing rn
/// Creates a fingerprint hash for a conversation by sorting and concatenating a list of usernames
/// and generating a SHA1 hash.
pub fn create_convo_fp(names: &mut Vec<&str>) -> String {
    names.sort(); // sorts the names (keep in mind that Uppercase will have priority over lowercase)

    let mut mash = String::new();
    for name in names {
        mash.push_str(name);
    }

    utils::calc_sha1(&mash)
}

/// Appends the fingerprint of a newly created conversation to a user's conversation array in the database.
pub fn update_user_convos(client: &Client, username: &str, oid: &ObjectId) -> Result<UpdateResult, MdbError>{
    let collection = client.database("qp").collection("users");

    let query = doc!{ "username": username };
    let mods = UpdateModifications::Document(doc!{ "$push": {"conversations": oid}}); //https://github.com/mongodb-labs/mongo-rust-driver-prototype/issues/287
    collection.update_one(query, mods, None)
}

/// Takes a vector of username strings, verifies the users all exist, creates a new conversation
/// document, and then appends the new doc's ObjectId to each user's conversations array.
pub fn new_convo(client: &Client, mut usernames: Vec<&str>) -> Result<(), NewConvoError> {
    // verify that each user exists
    for name in &usernames {
        match mongops::users::get_user(client, name) {
            Ok(success) => match success {
                Some(_) => (),
                None => return Err(NewConvoError::Dne(UserDneError{user: name.to_string()})),
            },
            Err(err) => return Err(NewConvoError::Mdb(err)),
        }
    }

    // create a new convo doc with an empty "participants" doc
    let collection = client.database("qp").collection("conversations");
    let mut convo_doc = doc! {
        "fingerprint": create_convo_fp(&mut usernames),
        "participants": {}, // empty doc
    };
    let mut participants = match convo_doc.get_document_mut("participants") {
        Ok(doc) => doc,
        Err(err) => return Err(NewConvoError::ValAcc(err)),
    };

    // append each username as a key in the participants doc, with an empty bson array as val
    for name in &usernames {
        match participants.insert(name.to_string(), Vec::<Bson>::new()) {
            Some(_) => (),
            None => (), // insert returns the old entry's value, and in this case we dgaf what it is
        };
    }

    let new_convo_id = match collection.insert_one(convo_doc, None) {
        Ok(new_doc) => {
            match new_doc.inserted_id {
                Bson::ObjectId(id) => id,
                _ => return Err(NewConvoError::BadId(BadInsertionError{}))
            }
        },
        Err(err) => return Err(NewConvoError::Mdb(err)),
    };

    // NEED TO HANDLE A CASE WHERE THERE ARE NO MATCHED DOCS; TODO
    // Append the returned ObjectId to each user's conversations array
    for name in &usernames {
        match update_user_convos(client, name, &new_convo_id) {
            Ok(_) => (),
            Err(err) => return Err(NewConvoError::UpConvo(UpdateConvosError{user: name.to_string(), error: err})),
        };
    }
    Ok(())
}
