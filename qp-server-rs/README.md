# QP Server - Rust Edition

I have just discovered that MongoDB has an alpha release of a driver for Rust... Which I will definitely be trying out now!

## Important Considerations

Keep in mind that the JSON used in MongoDB is just an exchange format. When querying the database, we are querying for an entire document each time, and then all queries within that document are handled within our code. *The performance considerations of each data structure used in our database will be reflected in our server side code*.

So basically, the arrays should be turned into sets within our code after the database has been queried for a document. Then we can search each user's unread array more quickly and determine which messages have expired (aka have been read by all participants).

## Next Steps

* URGENT: NEED TO RESTRUCTURE OBJECTS IN THE DATABASE TO REFERENCE USERS BY THEIR OBJECTIDS AND NOT THEIR USERNAMES. USING THE USERNAMES COULD BE ABSOLUTELY CATASTROPHIC IF USERS WANT TO DELETE THEIR ACCOUNTS OR CHANGE USERNAMES (ANOTHER USER CLAIMING THE OLD USERNAME COULD POTENTIALLY RECEIVE MESSAGES FROM SOMEONE ELSE'S CONVERSATION)

#### What do I need to do before QP Server is fleshed out enough and I can start working on the client again?

* Create users
* Create conversations between users
* Create new messages (which are then available to all members of a conversation)
* Remove messages from a user's unreceived (array of messages they haven't downloaded)
* Delete messages when they no longer appear in anyone's unreceived array  

## Resources

https://stackoverflow.com/questions/42584368/how-do-you-define-custom-error-types-in-rust
