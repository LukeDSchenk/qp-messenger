extern crate rand;
extern crate serde;
extern crate serde_json;

use std::fs;
use rand::Rng;
use itertools::izip;
use serde::{Serialize, Deserialize};
use crate::utils;

#[derive(Serialize, Deserialize)]
pub struct OneTimePad { // make these vars private later
    pub id: String,
    pub key: String,
    pub key_length: u32,
    pub offset: u32,
}

impl OneTimePad {
    /// Create a new OneTimePad with a given key length
    pub fn new(length: u32) -> OneTimePad {
        // calculate a hash of self.key and store it in self.id
        let key = OneTimePad::generate_random_key(length);
        let id = utils::calc_sha1(&key);

        OneTimePad {
            key: key,
            key_length: length,
            offset: 0,
            id: id,
        }
    }

    /// Read a new OneTimePad from a .json file
    // TODO: Return a Result to be handled by the main program
    pub fn from_json_file(filename: String) -> OneTimePad {
        //read the file to a json string
        let j = fs::read_to_string(filename).unwrap();
        //serialize and return a new OTP object from the json
        let new_otp: OneTimePad = serde_json::from_str(&j).unwrap();
        new_otp
    }

    /// Write a OneTimePad to a .json file
    // https://stackoverflow.com/questions/1510104/where-to-store-application-data-non-user-specific-on-linux
    // TODO: Modify to return a Result which can be handled in the main program
    //       Create a generic function that will write json files for any object with the serialize trait
    pub fn to_json_file(&self, filename: String) {
        // serialize the OneTimePad to JSON
        let j = serde_json::to_string(self).unwrap();
        // write the json to a file
        fs::write(format!("/opt/qp-messenger/{}", filename), j).expect("Unable to write to file");
    }

    /// Generates a cryptographic random key string of a given `length`
    /// TODO: sub in CSRNG
    pub fn generate_random_key(length: u32) -> String {
        let mut key = String::new();

        for _ in 0..length {
            let rand_int: u32 = rand::thread_rng().gen_range(0, 1114112);
            key.push(std::char::from_u32(rand_int).unwrap_or('�'));
        }

        key
    }

    /// Encrypts a message using this one-time pad's key, and updates the offset
    /// The message to be encrypted must be shorter than or equal to the length of the key
    pub fn encrypt_message(&mut self, message: String) -> Result<String, String> {
        let message_len = message.chars().count() as u32; // string.chars().count() is an O(n) operation due to unicode

        if (self.key_length - self.offset) < message_len {
            return Err(format!("Message cannot be longer than the length of the unused space in the encryption key (message is {} chars, key space is only {})", message_len, self.key_length));
        }

        let mut ciphertext = String::new();

        for (char_mess, char_key) in izip!(message.chars(), self.key.chars()) {
            let mut mess_int = char_mess as u32;
            let key_int = char_key as u32;
            mess_int = (mess_int + key_int) % 1114112;

            ciphertext.push(std::char::from_u32(mess_int).unwrap_or('�'));
        }
        self.offset += message_len; // update this OneTimePad offset so that bytes are not reused

        Ok(ciphertext)
    }

    /// Attempts to decrypt a message using the one-time pad's key
    /// TODO: add a function to determine whether or not a message was successfully decrypted
    pub fn decrypt_message(&self, ciphertext: String) -> Result<String, String> {
        let cipher_len = ciphertext.chars().count() as u32;

        if self.key_length < cipher_len {
            return Err(format!("Message cannot be longer than the encryption key; this message was likely encrypted with another key"))
        }

        let mut decrypted_message = String::new();

        for (char_ciph, char_key) in izip!(ciphertext.chars(), self.key.chars()) {
            let ciph_int = char_ciph as i32;
            let key_int = char_key as i32;
            //println!("ci32: {}\nki32: {}", ciph_int, key_int);
            let ciph_int = utils::modulo(ciph_int-key_int, 1114112);
            //println!("aftermath: {}", ciph_int);

            decrypted_message.push(std::char::from_u32(ciph_int).unwrap_or('�'));
        }

        Ok(decrypted_message)
    }
}

#[derive(Serialize, Deserialize)]
pub struct Message {
    id: String,
    otp_id: String, // id hash of the pad used to encrypt this message
    otp_index: (u32, u32), // (start, end) indices signifying the block of key used to encrypt the message
    ciphertext: String, // the encrypted message text
}

impl Message {
    pub fn from(text: String) -> Message {
        // select a key and encrypt the message text

        Message {
            id: String::from("filler"),
            otp_id: String::from("filler"),
            otp_index: (69, 69),
            ciphertext: text, // this will actually be our text after it's encrypted
        }
    }
}
