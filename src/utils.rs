extern crate crypto;

use crypto::digest::Digest;
use crypto::sha1::Sha1;

/// Modulo function for decryption; % in Rust is the remainder operator and does not work properly on negatives
pub fn modulo(x: i32, y: i32) -> u32 {
    let z = x % y;

    if z < 0 {
        return (z + y) as u32
    }
    z as u32
}

pub fn calc_sha1(data: &str) -> String {
    let mut hasher = Sha1::new();
    hasher.input_str(&data);
    let hex = hasher.result_str();
    hex
}
