mod utils;
mod qp_messenger;
mod sqlite;
//mod bundle;

use qp_messenger::OneTimePad;

fn test_crypt(message: String) {
    let mut pad = OneTimePad::new(138);
    let test = message;

    let crypt = match pad.encrypt_message(test.clone()) {
        Ok(ciphertext) => ciphertext,
        Err(err) => err,
    };

    let decrypt = match pad.decrypt_message(crypt.clone()) {
        Ok(decrypted_message) => decrypted_message,
        Err(err) => err,
    };

    println!(" Original: {}", test);
    println!("Encrypted: {}", crypt);
    println!("Decrypted: {}\n", decrypt)
}

fn test_json_writing() {
    let pad = OneTimePad::new(143);
    pad.to_json_file(String::from("key1.json"));
}

fn test_json_reading() -> OneTimePad {
    let pad = OneTimePad::from_json_file(String::from("/var/lib/qp-messenger/key1.json"));
    pad
}

fn test_json_writing_proper() {
    let pad = OneTimePad::new(2000);
    pad.to_json_file(String::from("proper_test_1.json"));
}

fn test_offset() {
    let mut pad = OneTimePad::new(2048);
    let str1 = String::from("This is a test yeeeehaw: 😍😍😍😱😱😱🤢🤢💩💩\n🔥😘👍🥰😎😆👏😇🌹🤦❤😍");
    println!("{}", str1);
    println!("Offset before encrypting message: {}", pad.offset);

    let ciphertext = match pad.encrypt_message(str1.clone()) {
        Ok(ciphertext) => ciphertext,
        Err(err) => panic!(err),
    };
    println!("Cipher text: {}", ciphertext);

    println!("Offset after encrypting message: {}", pad.offset);
    println!("Message len: {}", str1.chars().count());

    let decrypted_message = match pad.decrypt_message(ciphertext) {
        Ok(mess) => mess,
        Err(err) => panic!(err),
    };
    println!("Decrypted: {}", decrypted_message);
}

// This function shows that our sha1 util is working properly
fn test_sha1() {
    let mut pad = OneTimePad::new(138);
    println!("Pad key: {} \nPad ID: {}", pad.key, pad.id);
    let equal = utils::calc_sha1(&pad.key) == pad.id;
    println!("{}", equal);
}

fn test_init_sqlite() {
    let result = sqlite::init_sqlite();
    match result {
        Err(err) => println!("A SQLite error occurred: {}", err),
        Ok(()) => println!("Tables created successfully!")
    };
}

fn run_tests() {
    test_crypt(String::from("This is a proper string test motherfucker!!! 😱😱😱😱😱😱😱😱🤢🤢🤢🤢🤢🤢🤢🤢💩💩💩💩💩💩💩💩💩💩"));
    test_crypt(String::from("the quick brown fox jumped over the lazy dog 1234567 times and then it just flat out dieded (yes: I realize that this is so dumb &*&*&*&*)"));
    test_crypt(String::from("💩💩💩💩"));
    test_crypt(String::from("🔥😘👍🥰😎😆👏😇🌹🤦❤😍"));
    test_crypt(String::from("😍😍😍😍😍😍😍😍😍😍😍😍"));
    /*test_json_writing_proper();
    let new_pad = test_json_reading_proper();🔥😘👍🥰😎😆👏😇🌹🤦❤😍🔥😘👍🥰😎😆👏😇🌹🤦❤😍
    new_pad.encrypt_message(String::from("This is a test yeeeehaw: 😍😍😍😱😱😱🤢🤢💩💩\n🔥😘👍🥰😎😆👏😇🌹🤦❤😍"));

    println!("All tests have passed!");*/
    test_offset();
}

fn main() {
    //test_init_sqlite();
    test_crypt(String::from("NG."));
    run_tests();

}
