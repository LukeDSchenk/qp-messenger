// This file will contain all structs and functions regarding the soon-to-be SQLite backend for
// QP Messenger. SQLite should provide better performace, easier maintenance, and an overall much
// cleaner and more elegant coding experience.
extern crate rusqlite;

use rusqlite::{params, Connection, Result};
use rusqlite::NO_PARAMS;

/*
Some example SQLite statements
---------------------------------------------------------------------------------------------------

create table if not exists keys (
id text not null unique,
data text not null,
convo_id text not null,
);

insert into keys (id, data, convo_id)
  values ('a7e9a9b9f09e023d90c0436e76b', 'encryption key data would be here', 'convo id here 134ea9efeb8b9d9c90');

---------------------------------------------------------------------------------------------------

create table if not exists messages (
   id text not null unique,
   convo_id text not null,
   data text not null,
   key_id text not null,
   key_start_idx int not null,
   key_end_idx int not null);

insert into messages (id, convo_id, data, key_id, key_start_idx, key_end_idx)
  values ('23423fdefasampleid80asdfas0', 'fsdalfj23rqfconvoidasodf023', 'encryptedmessagedata9097sadfasfhsadf', 'keyid2308423sadfa', 3, 37);

*/

pub fn init_sqlite() -> Result<()> {
    let conn = Connection::open("/opt/qp-messenger/qp.db")?; // open() creates a db if it doesn't exist

    // Create necessary tables
    conn.execute(
        "create table if not exists keys (
            id text not null unique,
            data text not null,
            convo_id text not null
        );",
        NO_PARAMS,
    )?;

    conn.execute(
        "create table if not exists messages (
            id text not null unique,
            convo_id text not null,
            data text not null,
            key_id text not null,
            key_start_idx int not null,
            key_end_idx int not null
        );",
        NO_PARAMS,
    )?;
    Ok(())
}
