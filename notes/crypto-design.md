# Cryptography Design

1. Users will create a new **bundle** (a set of one time pads) and choose the number of OTPs, the length of the keys and the recipient.
    * The length of the encryption keys will determine the maximum size of the messages that can be sent
2. The bundle will then query the server for that recipient's public key, and encrypt the bundle file with it. 
    * Encrypting the bundle will make it much harder to read the keys if they are to be intercepted in transit, since the thief would need the recipient's private key to decrypt them.
    * The client app will generate the pub/private keys, but only the public key will be sent and stored in the server.
3. Bundle files can then be transferred to their recipients by any means chosen by the users.
    * The recommended method will be physically in person, using something such as a USB drive or whatnot.
    * Another recommendation could be that if you have to transfer bundles over the wire, try and do so in a way that is anonymous and nonpermanent (such as through signal disapperaing messages, etc.)
    * Obviously it should be noted that the keys are only as secure as you make them, but such is the case for any type of encryption (no encryption will prevent information theft due to user error).
    
## Very Very Important Finding!

See: [Key Distribution](https://en.wikipedia.org/wiki/One-time_pad#Key_distribution)

"However, once a very long pad has been securely sent (e.g., a computer disk full of random data), it can be used for numerous future messages, until the sum of their sizes equals the size of the pad."

This means that it doesn't matter how many OTP files are generated, but what does matter is the *total character length*. 
Instead of messages containing just the hashed ID of the key file used, it would also contain a record of the beginnging and ending indices of the characters used to encrpyt a given message.
Holy shit there is so much that goes into this.
