## Notes regarding SQLite DB and structure

Users will have unique IDs. Conversations between two people will have unique IDs. Each conversation will be a tie between two people. Each key (one-time pad) has a unique ID (a SHA1 hash as of now). Keys will be generated for specific conversations, and thus will be related to a specific conversation in the database. Messages will also be tied to specific conversations via a relationship.

Cascade should be enabled, and thus deleting all history for a specific conversation would also delete:

  * The reference to the other user's ID
  * All keys associated with that conversation
  * All messages associated with that conversation

## What will the local SQLite DB look like?

Let's stop getting too caught up in specifics and instead let's make a simple version of what we want to do:

1. Users will generate long ass keys for encrypting messages to certain user groups (user groups can be one or many people)
  * For now, user groups will simply serve as a way of organizing keys during storage and encryption so that keys will not be reused for different intended recipients
2. Users will write messages and encrypt them with a key; the key will be stored in the message metadata
3. Users will send messages to other users; for now it will be up to them to figure out how, but soon I will implement a server for sending the messages
  * Messages can truly be shared in any way, including publicly, because the encryption method provides zero context
  * Before encryption, messages should have the sender's username appended to the metadata
4. Users will share keys with other users who are members of certain user groups

## Example usage in CLI form

Users will start the qp-messenger app in the terminal, and they will be greeted with a simple text UI:

```bash
**QP-Messenger Art Here**

1. Decrypt Messages
2. Create messages
3. Manage Groups
4. Generate Keys
```

## What do locally stored keys need?

Local keys will need:

1. The key data itself
2. A hashed ID value of the key
3. A group/conversation ID, where that conversation references the intended recipients
