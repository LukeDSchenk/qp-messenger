## Random notes

#### What do I have?

* Create a OneTimePad key
* Use that key to encrypt and decrypt messages

#### What do I need?

1. Method of writing long keys to files in a way that they can be indexed
  * Use JSON format
    * Sample keystore format: {id: "a_hash_of_the_key", length: 9999, offset: 3445, key: "the actual random key string"}
      * Where offset is the last index in that pad that has already been used to encrypt
    * Sample messagestore format: {id: "a hash of the message", key_id: "hash of the key used to encrypt this message", key_index: [21, 997]}
      * Where key_index is the first and last char used to encrypt the message

2. When deleting old keys, automatically delete all messages that were encrypted with that key (because they are now no longer readable)


## What will it be like to use the messenger?

"User <username> has sent you a message! Exchange key files to decrypt them."

*User clicks the "decrypt" button on the message"*

**Response 1:** Key file could not be found, have you exchanged keys for this conversation?

**Response 2:** *Key file is found and the message is decrypted and displayed normally*

* Keys will be located by their key hashes within the json files, but after being found the hash will be calculated manually by the recipient's client app to further validate the key

## Current test case

1. Create a new random key, store it in a file /var/lib/qp-messenger/key1.json
2. Create a new message and encrypt it with the key
3. Update the offset of the key (to verify which parts of the key have been used)


## Keys with hash reversing possibility: potential solutions

##### Option 1: Assign keys a random ID value

Pros:

* random ID value gives no context to the contents of the key

Cons:

* [**IMPORTANT**] Keys can no longer be rehashed for validation (**but on the other hand, revalidation by hashing *shouldn't matter* as long as the entire files sending the keys have been signed by the sender and validated that way... There is no logical reason why a sender would want to tamper with their own keys and basically make their own messages unreadable**)
