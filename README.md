# QP Messenger
"Quantum Proof" Messenger is an encrypted private messenger that works by using One Time Pad encryption. [One Time Pad encryption](https://en.wikipedia.org/wiki/One-time_pad) is theoretically impossible to crack by any amount of computing power, assuming that it is done correctly.

# Notice

This README was written a while back, and is currently just a place where I have spilt my brain. Please do not take everything in here as fact, as I have decided on many design
changes throughout the development process.

## How will it work?

Users will use NFC to generate and share a number of OTPs (this number is up to them to decide). After generating OTPs, users will airdrop or touch phones or whatever to share them for a set number of time. I have not yet decided how the program will decide which OTP to use for each day (could be simply based off of what day it is, or maybe allowing some custom criteria from the user). Another issue is that the OTPs need to be stored in a secure manner on the devices.

* Maybe find a way to securely allow users to write a method for generating OTP keys. It seems risky.

## Roadmap

This roadmap is a very rough plan, and right now is kind of just a place to throw some thoughts

### V1: CLI Messenger (leaving this for reference, but this has changed considerably. I will make an updated timeline soon)

The first version of the messenger will be a CLI only tool. Here are some potential features:

* Users can generate a zip file of a folder containing archives of generated random one time pads
* The clients will handle the pulling of one time pads from the archive and encrypting messages
* The receiver's client will use the same algorithm to determine which OTP is the decryption key, and use it accordingly.
  * How ling will a OTP be valid? How will I handle the creation and partitioning of them so to speak?
* Try giving each OTP a unique ID, and thus messages can be sent with a pad ID attached
  * Upon usage of a One Time Pad, a user's device will know to delete it
* One time pads will be encrypted with a password or something like that after they are imported onto a users machine
* Users can choose how many OTPs to generate in one bundle, and how many characters will make up each
  * The app can provide a counter of how many encrypted messages they have left before needing to renew their OTPs

## Resources

* [One-time pad: Wikipedia](https://en.wikipedia.org/wiki/One-time_pad)
* [Signal Messenger Architecture](https://www.sorincocorada.ro/signal-messanger-architecture/)
* [Crypton: Secure Anonymous SMS in the Cloud](https://crypton.sh/#)
* [Stack Overflow post regarding modules/imports](https://stackoverflow.com/questions/46829539/how-to-include-files-from-same-directory-in-a-module-using-cargo-rust)
* [I Just Hit $100k/year on GitHub Sponsors](https://news.ycombinator.com/item?id=23613719)
