//Note from Rust documentation:
//    All Unicode Char values are valid as u32s; however, not every u32 is a valid Unicode Char

// Perhaps to avoid this issue, messages will have to be encrypted and stored as numbers.
// Or, the values will have to go through a check to see if they are valid unicode, and if they aren't,
// they will need to be somehow just stored as a Hex value.
// https://stackoverflow.com/questions/4850241/how-many-bits-or-bytes-are-there-in-a-character

// Okay, so it turns out that two chars can indeed be represented by the same symbol, but contain
// completely different hex values within them. It is likely that my One-Time Pads and ciphertexts
// will contain a lot of 􏪪 (unassigned unicode values).

fn main() {
    /*let x: char = 'T';
    println!("{}", x);
    let y = x as u32;
    println!("{}", y);

    let z: u32 = 32;
    println!("\n{}", z);*/

    //🐈 = U+1F408
    //🐉 = U+1F409
    let cat_char = '🐈';
    let cat_code = '\u{1F408}';
    println!("Cat: {}\nCat from code: {}\n", cat_char, cat_code);

    let cat_int = cat_char as u32;
    println!("Cat as u32: {}", cat_int);
    let drag_char = std::char::from_u32(cat_int + 1).unwrap_or('�'); // if from_u32 contains None (not Some(x)), set the char as �
    println!("Dragon (Cat + 1): {}", drag_char);

    let ent = '\n';
    println!("\nEnter as u32: {}", ent as u32);

    // valid unicode hex values range from U+000000 to U+10FFFF
    let test1 = '\u{10FFFF}'; // 1,114,111 in decimal
    let test2 = '\u{10FAAA}';
    println!("\n{}: {}", test1, test1 as u32);
    println!("\n{}: {}", test2, test2 as u32);
}
