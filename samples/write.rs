use std::fs;

// Simple way to write strings to a file
// Note that this will create the given file if it does not already exist

fn main() {
    let data = String::from("Some data!");
    fs::write("./test.txt", data).expect("Unable to write to file");
}
