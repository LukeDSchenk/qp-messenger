package mongo

import (
    "time"
    "context"
    "go.mongodb.org/mongo-driver/mongo"
    "go.mongodb.org/mongo-driver/mongo/options"
    "go.mongodb.org/mongo-driver/mongo/readpref"
    //"go.mongodb.org/mongo-driver/bson"
    "go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
    ID            primitive.ObjectID   `bson:"_id,omitempty"` //omitempty means the field won't exist if it's empty
    Username      string               `bson:"username"`
    Password      string               `bson:"password"`
    Conversations []primitive.ObjectID `bson:"conversations"`
}

type Conversation struct {
    ID         primitive.ObjectID `bson:"_id,omitempty"`
}

// Opens a connection to a MongoDB instance via a connection string
// Then pings the server to verify the connection
func Test_connection(mongodb_uri string) (*mongo.Client, error) {
    client, err := mongo.NewClient(options.Client().ApplyURI(mongodb_uri))
    if err != nil {
        return client, err
    }

    ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
    err = client.Connect(ctx)
    if err != nil {
        return client, err
    }

    // Connect() does not block for server discovery
    // Ping() is used to verify if a MongoDB server has been found and connected to
    ctx, _ = context.WithTimeout(context.Background(), 2*time.Second)
    err = client.Ping(ctx, readpref.Primary())
    if err != nil {
        return client, err
    }

    // return the client object if no errors occur during connection
    return client, nil
}

// Modify all new/create functions to return the ID of the documents they are creating

// Attempts to create a new user, returns an error if the user already exists
func Create_user(client *mongo.Client, username string, password string) error {
    collection := client.Database("qp").Collection("users")

    new_user := User {
        Username: username,
        Password: password,
    }

    ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
    //bson.M{"username": username, "password": password, "conversations": bson.A{}}
    _, err := collection.InsertOne(ctx, new_user)
    if err != nil {
        return err
    }

    //id := res.InsertedID // replace _ with res
    //fmt.Println("Created a new user with ID: ", id)

    return nil
}

// should participants be a list of user IDs or usernames?
// why don't we try usernames first for simplicity
func New_Conversation(client *mongo.Client, participants []string) error {
    return nil
}
