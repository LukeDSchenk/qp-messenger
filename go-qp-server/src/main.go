package main

import (
    "context"
    "log"
    "time"
    "./mongo"
)

func main() {
    ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
    client, err := mongo.Test_connection("mongodb://localhost:4444")
    if err != nil {
        log.Fatal(err)
    }
    defer client.Disconnect(ctx)

    err = mongo.Create_user(client, "DUMBASSUSER", "WITHADUMBASSPASSWORD")
    if err != nil {
        println(err.Error()) //multiple write errors: [{write errors: [{E11000 duplicate key error collection: qp.users index: username dup key: { : "nineofpine" }}]}, {<nil>}]
    }
}
