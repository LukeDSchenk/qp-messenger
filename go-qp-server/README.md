# qp-server

qp-server refers to the backend that will handle storage of user data and delivery of messages via a MongoDB (tentative) instance. As of right now, the plan is to write qp-server in Go or Python.

## To Do

Restructure the code to model the MongoDB documents as Go structs. This means creating a struct to model users, conversations, and messges. See [this](https://www.mongodb.com/blog/post/quick-start-golang--mongodb--modeling-documents-with-go-data-structures) article for more information on modeling documents with Go data structures. You will need to use slices of primitive.ObjectID objects ([]primitive.ObjectID). Overall, should make writing and querying the collections pretty simple now that I understand which data structure to use.

## Useful links

https://dev.to/yasaricli/getting-mongodb-id-for-go-4e05
https://www.mongodb.com/blog/post/quick-start-golang--mongodb--how-to-read-documents
https://www.mongodb.com/blog/post/quick-start-golang--mongodb--modeling-documents-with-go-data-structures **READ THIS ONE NEXT**
https://blog.ruanbekker.com/blog/2019/04/17/mongodb-examples-with-golang/
